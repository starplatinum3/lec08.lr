//    Translation.java
// Translation class for simple example to convert infix exprs to postfix
// March 2003, sdb

package postfix;
import postfix.analysis.*;	// needed for DepthFirstAdapter
import postfix.node.*;		// needed for syntax tree nodes. 

class Translation extends DepthFirstAdapter 
{ 

public void outAPlusExpr(APlusExpr node) 
{// out of alternative {plus} in expr, we print the plus. 
   System.out.print ( " + ");
}

public void outAMinusExpr(AMinusExpr node) 
{// out of alternative {minus} in expr, we print the minus. 
   System.out.print ( " - ");
}

public void outAMultTerm(AMultTerm node) 
{// out of alternative {mult} in term, we print the minus. 
   System.out.print ( " * ");
}

public void outADivTerm(ADivTerm node) 
{// out of alternative {div} in term, we print the minus. 
   System.out.print ( " / ");
}

public void outANumberFactor (ANumberFactor node)
//  out of alternative {number} in factor, we print the number.
{   System.out.print (node + " "); }


}
