package decaf;
import decaf.lexer.*;
import decaf.node.*;
import java.io.*;

class Test

{  

public static final int x=2, y=3;

public static void main (String [] args)
{
	Lexer lexer = new Lexer 
		(new PushbackReader 
			(new InputStreamReader (System.in), 1024));

// GenericToken token = new GenericToken();

try { 
	while (true)
	    {
		System.out.println ( lexer.next()) ; 
		System.out.println (lexer.peek()) ;
	    }
    }
catch (LexerException le)
    { 	System.out.println ("Lexer exception: " + le); }
catch (IOException ioe)
    {	System.out.println ("IO Exception: " + ioe); }

}

}
