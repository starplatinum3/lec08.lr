///////////////////////////////////////////////////
//  Added March 2003 for compatibility with decaf
//  sdb
#include <stdio.h>
///////////////////////////////////////////////////


/* Size of hash table for identifier symbol table */
#define HashMax 100

/* Size of table of compiler generated address labels */
#define MAXL 1024

/* memory address type on the simulated machine */
typedef unsigned long ADDRESS;  	

/* Symbol table entry */
struct Ident 
	{char * name;
	struct Ident * link;
	int type; /* function name = 1,
		     int = 2,
		     float = 3 */
	ADDRESS memloc;};

/* Symbol table */
struct Ident * HashTable[HashMax];

int dcl = TRUE;	/* processing the declarations section */
int identType;  /* type used in processing a declaration */

/* Binary search tree for numeric constants */
struct nums
	{ADDRESS memloc;
	struct nums * left;
	struct nums * right;};
struct nums *  numsBST = NULL;

/* Record for file of atoms */
struct atom
	{int op;		/* atom classes are shown below */
	ADDRESS left;
	ADDRESS right;
	ADDRESS result;
	int cmp;		/* comparison codes are 1-6 */
	int dest;
	};
	
/* ADD, SUB, MUL, DIV, and JMP are also atom classes */
/* The following atom classes are not op codes */
#define NEG 10
#define LBL 11
#define TST 12
#define MOV 13

FILE * atom_file_ptr;
FILE * data_file_ptr;		// file containing constants
ADDRESS avail = 0, end_data = 0;
int err_flag = FALSE;		/* has an error been detected? */

