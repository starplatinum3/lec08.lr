# **2021-22学年第2学期**

## **实 验 报 告**

![zucc](zucc.png){width="1.5208333333333333in" height="1.5208333333333333in"}

-   课程名称: <u>编程语言原理与编译</u>

-   实验项目: <u>语法分析 `LR`分析</u>

-   专业班级: <u>计算1903</u>

-   学生学号: <u>31901077</u>

-   学生姓名: <u>缪奇鹏</u>

-   实验指导教师: <u>张芸</u>

## 实验内容

1. 阅读ppt,阅读教材第3章

- 理解LR（0） DFA的构建过程

- 理解如何从DFA状态图，进行LR分析表的构建

- #### 教材 p50 3.4.3 理解冲突产生原因

- •**归约可能会遇到的问题**
  
  •**在第二步归约的时候分析栈的情况是**
  
  •**有两种归约的可能****:**
  
  •**A****→Ab和A→b
  
- 
  
  - #### p51 理解`图3-13` `LR 状态表`，找到其中的冲突项
  
  - Yace能指出移进-归约冲突和归约-归约冲突。移进-归约冲突是在移进和归约之间进
    行的一种选择;归约-归约冲突是使用两条不同规则进行归约的一种选择。默认情况下，
    Yacc选择移进来解决移进-归约冲突，选择使用在文法中先出现的规则来解决归约-归约冲突。
    对于文法3-11，Yacc 报告它有一个移进-归约冲突。任何冲突都指出这个分析可能不是文
    法设计者所预期的，因此应当引起重视。通过阅读Yacc生成的详细的描述文件可以查看冲突,
    图3-13展示了这个文件。
    简单地查看--下状态17便可看出这个冲突是由常见的悬挂else问题引起的。因为Yacc解
    决移进-归约冲突的默认做法是移进，而这个移进的结果正好与所希望的使else与最近的
    then匹配相符合，因此这个冲突不会有损害。
    当移进-归约冲突对应的是一种很明确的情形时，文法中可接受这种冲突。但是多数移
    进-归约冲突和所有的归约-归约冲突都会带来严重的问题，因此应通过重写文法来消除它们。
  
  - ##### 找到其中的 `Action Table（动作表）` ，`Goto Table（状态转换表）`的定义
  
  - 分析表由action表和goto表两部分组成:
    action[sm, ai]: 表示当状态sm面临输入ai时的动作动作主要包括四种:
  
    Shift s(移进): 将ai和s=action[sm, ai]压入栈中
  
    Reduce A→β(归约): 用产生式A→β归约 
  
    Accept(接受): 停机，接受，成功
  
    Error(报错): 调用出错处理
  
- 教材 p50 3.4.2.优先级指导 
  - 理解在语法说明文件中，优先级的指定方式 
    - ###### 什么时左/右结合/非结合 ，如何在语法说明文件里面声明 p53
    
    - 左结合：从左往右运算
    
      右结合：从右向左运算
    
      非结合：不进行集合
    
      声明：
    
      使用%nonassoc声明非结合
    
      使用%left声明左结合
    
      使用%right声明右结合
    
      指出:+和~是左结合的且具有相同的优先级;*和/是左结合的且它们的优先级高于+; ^是右结含的且具有最高优先级;=和≠是非结合的，它们的优先级低于+
      
    - ###### 如何用 `%prec` 指示，自定义某规则的优先级 p53
    
    - 替代使用“规则具有其最后一个单词的优先级”的默认约定，我们可以用%prec指导命令给规则指定--种明确的优先级。这种方法常用于解决“一元负运算”问题。在大多数程序设计语言中，一元负运算的优先级要高于任何一个二元操作符的优先级，所以`–6*8`被分析成`(-6)*8`，而不是`–(6*8)`。
    
  - [http://mdaines.github.io/grammophone/#](http://mdaines.github.io/grammophone/) 核对你的作业
  - 请勿抄袭


2. 设有如下文法 

```sh
S -> S A b .
S -> a c b .
A -> b B c .
A -> b c .
B -> b a .
B -> A c .
```
分析栈上的内容如下,请分别写出可归约串是什么（▽ 表示栈底）：

可能就是右边的转化为非终止符

```sh
(a)▽SSAb
SAb 可以是 S -> S A b，所以可规约串是 SAb
(b)▽SSbbc
A -> b c ，所以是 bc
(c)▽SbBc
bBc
(d)▽Sbbc
bc
```

   3.设有如下输入串，请用2中的文法，采用 shift/reduce分析下面的串。

   请按ppt 中 构造表格，列出 

   分析栈，输入流， shift/reduce操作 的内容

```sh
(a) acb
(b) acbbcb
(c) acbbbacb
(d) acbbbcccb
(e) acbbcbbcb
```

| 符号栈 | 输入串 | 动作           |
| ------ | ------ | -------------- |
| $      | acb$   | 移进           |
| $a     | cb$    | 移进           |
| $ac    | b$     | 移进           |
| $acb   | $      | 归约S -> a c b |
| $S     | $      | 接受           |

| 符号栈 | 输入串  | 动作           |
| ------ | ------- | -------------- |
| $      | acbbcb$ | 移进           |
| $a     | cbbcb$  | 移进           |
| $ac    | bbcb$   | 移进           |
| $acb   | bcb$    | 归约S -> a c b |
| $S     | bcb$    | 移进           |
| $Sb    | cb$     | 移进           |
| $Sbc   | b$      | 规约 A -> b c  |
| $SA    | b$      | 移进           |
| $SAb   | $       | 规约S -> S A b |
| $S     | $       | 接受           |

| acbbbacb |           |                 |
| -------- | --------- | --------------- |
| 符号栈   | 输入串    | 动作            |
| $        | acbbbacb$ | 移进            |
| $a       | cbbbacb$  | 移进            |
| $ac      | bbbacb$   | 移进            |
| $acb     | bbacb$    | 归约S -> a c b  |
| $S       | bbacb$    | 移进            |
| $Sb      | bacb$     | 移进            |
| $Sbb     | acb$      | 移进            |
| $Sbba    | cb$       | 归约 B -> b a   |
| $SbB     | cb$       | 移进            |
| $SbBc    | b$        | 归约 A -> b B c |
| $SA      | b$        | 移进            |
| $SA b    | $         | 归约 S -> S A b |
| $S       | $         | 接受            |

| 符号栈 | 输入串     | 动作              |
| ------ | ---------- | ----------------- |
| $      | acbbbcccb$ | 移进              |
| $a     | cbbbcccb$  | 移进              |
| $ac    | bbbcccb$   | 移进              |
| $acb   | bbcccb$    | 规约 S -> a c b . |
| $S     | bbcccb$    | 移进              |
| $Sb    | bcccb$     | 移进              |
| $Sbb   | cccb$      | 移进              |
| $Sbbc  | ccb$       | 规约 A -> b c .   |
| $SbA   | ccb$       | 移进              |
| $SbAc  | cb$        | 规约 B -> A c .   |
| $SbB   | cb$        | 移进              |
| $SbBc  | b$         | 规约 A -> b B c . |
| $SA    | b$         | 移进              |
| $SAb   | $          | 规约 S -> S A b . |
| $S     | $          | 接受              |



| 符号栈 | 输入串     | 动作              |
| ------ | ---------- | ----------------- |
| $      | acbbcbbcb$ | 移进              |
| $a     | cbbcbbcb$  | 移进              |
| $ac    | bbcbbcb$   | 移进              |
| $acb   | bcbbcb$    | 规约 S -> a c b . |
| $S     | bcbbcb$    | 移进              |
| $Sb    | cbbcb$     | 移进              |
| $Sbc   | bbcb$      | 规约 A -> b c     |
| $SA    | bbcb$      | 移进              |
| $SAb   | bcb$       | 规约 S -> S A b   |
| $S     | bcb$       | 移进              |
| $Sb    | cb$        | 移进              |
| $Sbc   | b$         | 规约 A -> b c     |
| $SA    | b$         | 移进              |
| $SAb   | $          | 规约 S -> S A b . |
| $S     | $          | 接受              |

4. 设有如下文法和输入串，请说明是否有shift/reduce冲突 或者 reduce/reduce 冲突

```sh
S -> S a b .
S -> b A .
A -> b b .
A -> b A .
A -> b b c .
A -> c .
```

输入串
```sh
    (a) b c
    (b) b b c a b
    (c) b a c b

```

| 符号栈 | 输入串 | 动作                                     |
| ------ | ------ | ---------------------------------------- |
| $      | bc$    | 移进                                     |
| $b     | c$     | 移进                                     |
| $bc    | $      | 规约A -> c                               |
| $bA    | $      | reduce/reduce 冲突.S -> b A . A -> b A . |

| 符号栈 | 输入串 | 动作                                          |
| ------ | ------ | --------------------------------------------- |
| $      | bbcab$ | 移进                                          |
| $b     | bcab$  | 移进                                          |
| $bb    | cab$   | shift/reduce冲突  可以移进c或者规约A -> b b . |

| 符号栈 | 输入串 | 动作         |
| ------ | ------ | ------------ |
| $      | bacb$  | 移进         |
| $b     | acb$   | 移进         |
| $ba    | cb$    | 移进         |
| $bac   | b$     | 规约A -> c . |
| $baA   | b$     | 移进         |
| $baAb  | $      | 接受         |

5. 阅读lecture03.p31.fsyacc.pdf p31页 掌握fslex,fsyacc使用

   ![image-20220512214656858](lab08.LR.assets/image-20220512214656858.png)

   ![image-20220512214733490](lab08.LR.assets/image-20220512214733490.png)

   ![image-20220512214828991](lab08.LR.assets/image-20220512214828991.png)

   ![image-20220512214953683](lab08.LR.assets/image-20220512214953683.png)

   阅读 calcvar 中

- 词法说明 [lexer.fsl](https://gitee.com/sigcc/plzoofs/blob/master/calcvar/lexer.fsl)
- 语法说明 [parser.fsy](https://gitee.com/sigcc/plzoofs/blob/master/calcvar/parser.fsy)
- 调试运行代码
- 理解优先级指导的写法
  - 阅读 [ReadME]( https://gitee.com/sigcc/plzoofs/blob/master/calcvar/README.markdown)

6. plzoofs [calcvar](https://gitee.com/sigcc/plzoofs/tree/master/calcvar)项目，给`fsyacc` 工具添加 `-v` 参数，查看生成语法分析器的 LR 状态表
```html
// calcvar.fsproj 
<FsYacc *Include*="parser.fsy">
    <OtherFlags> -v --module Parser</OtherFlags>
</FsYacc>
```
注意下特定状态的
- action table

- goto table

- ```
  dotnet "C:\Users\25004\.nuget\packages\fslexyacc\10.2.0\build\fsyacc\netcoreapp3.1\fsyacc.dll" -v --module CPar parser.fsy
  ```

  

- ![image-20220512215435080](lab08.LR.assets/image-20220512215435080.png)

![image-20220512215523578](lab08.LR.assets/image-20220512215523578.png)


7. 阅读Fun语言中

- 词法说明 [FunLex.fsl](https://gitee.com/sigcc/plzoofs/blob/master/Fun/FunLex.fsl)
- 语法说明 [FunPar.fsy](https://gitee.com/sigcc/plzoofs/blob/master/Fun/FunPar.fsyfsl)
- 调试运行代码
- ![image-20220512215703281](lab08.LR.assets/image-20220512215703281.png)
- 同上`fsyacc`工具添加 `-v`查看 `LR` 分析状态表
- 
- ![image-20220512215826769](lab08.LR.assets/image-20220512215826769.png)

8. 阅读MicroC 语法分析器

- https://gitee.com/sigcc/plzoofs/blob/master/microc/CPar.fsy
- 由于 `C` 语言的指针，数组语法分析比较复杂，构造语法树时用到了比较高级的函数式编程技巧
- 大家慢慢理解

9. Fsharp参考案例（自选）

   -   Postfix/  后缀式 运算 1 2 + 3 *
   -   Usql/   sql 语言语法解析

​    

​    

## 参考资料

​    

   http://bb.zucc.edu.cn/webapps/cmsmain/webui/users/j04014/PLC/book

   flex与bison 中文版 第二版 高清.pdf

### 提交方式

- 打包`zip`上传到`bb`

- 实验报告采用`Markdown`格式

- `zip`内容包括`Markdown`文本、代码、部分体现实验过程的典型截屏(`.png`格式)

  